Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: powerdevil
Source: http://depot.kde.org/unstable/plasma/4.97.0/powerdevil-4.97.0.tar.xz

Files: *
       debian/*
Copyright: © 2008-2011 Dario Freddi <drf@kde.org>
           © 2009-2013 Chusslove Illich <caslav.ilic@gmx.net>
           © 2006, 2008 Kevin Ottens <ervin@kde.org>
           © 2009-2011 Dalibor Djuric <daliborddjuric@gmail.com>
           © 2010-2013 Lukáš Tinkl <ltinkl@redhat.com>
           © 2010, 2012-2013 Alejandro Fiestas <alex@eyeos.org> <afiestas@kde.org>
           © 2008-2014 Stefan Asserhall <stefan.asserhall@bredband.net>
           © 2014 Eloy Cuadra <ecuadra@eloihr.net>
           © 2014 Andrej Mernik <andrejm@ubuntu.si>
           © 2014 André Marcelo Alvarenga <alvarenga@kde.org>
           © 2008-2014 Yuri Chornoivan <yurchor@ukr.net>
           © 2008-2014 Bjørn Steensrud <bjornst@skogkatt.homelinux.org>
           © 2009 Benjamin K. Stuhl <bks24@cornell.edu>
           © 2010-2014 Lasse Liehu <lasse.liehu@gmail.com>
           © 2014 Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>
           © 2014 Freek de Kruijf <freekdekruijf@kde.nl>
           © 2014 Roman Paholík <wizzardsk@gmail.com>
           © 2008-2009, 2014 Sönke Dibbern <s_dibbern@web.de>
           © 2009-2012 Manfred Wiese <m.j.wiese@web.de>
           © 2009-2010, 2012 Tommi Nieminen <translator@legisign.org>
           © 2008-2013 Josep Ma. Ferrer <txemaq@gmail.com>
           © 2010-2011 Jorma Karvonen <karvonen.jorma@gmail.com>
           © 2009-2011 Eirik U. Birkeland <eirbir@gmail.com>
           © 2009 Karl Ove Hufthammer <karl@huftis.org>
           © 2009 Christian Esken <christian.esken@arcor.de>
           © 2012 Michael Zanetti <mzanetti@kde.org>
           © 2009 Aurélien Gâteau <agateau@kde.org>
           © 2009 Dario Andres Rodriguez <andresbajotierra@gmail.com>
           © 2014 Volkan Gezer <volkangezer@gmail.com>
           © 2010 Felix Geyer <debfx-kde@fobos.de>
           © 2010 Sebastian Kugler <sebas@kde.org>
           © 2014 Marce Villarino <mvillarino@gmail.com>
           © 2010 Rafael Fernández López <ereslibre@kde.org>
           © 2010 MetalWorkers Co."),
           © 2008 Teemu Rytilahti <teemu.rytilahti@d5k.net>
           © 2010 KDE Power Management System's
           © 2010 Power Management
           © 2013 Antoni Bella Pérez <antonibella5@orange.es>
           2014 Scarlett Clark <scarlett@scarlettgatelyclark.com>
License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".



Files: daemon/backends/hal/*
       daemon/backends/upower/* excluding:
        -? daemon/backends/upower/udevqt.h
        -+ daemon/backends/upower/udevqt_p.h
        -+ daemon/backends/upower/udevqtclient.cpp
        -+ daemon/backends/upower/udevqtclient.h
        -+ daemon/backends/upower/udevqtdevice.cpp
        -+ daemon/backends/upower/udevqtdevice.h
        -+ daemon/backends/upower/xlibandxrandr.h
        -+ daemon/backends/upower/xrandrxcbhelper.cpp
        -+ daemon/backends/upower/xrandrxcbhelper.h
Copyright: 2006 Kevin Ottens <ervin@kde.org>
           2010 Lukas Tinkl <ltinkl@redhat.com>
License: LGPL-2
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License version 2 as
 *   published by the Free Software Foundation
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

Files: daemon/backends/upower/xlibandxrandr.h
       daemon/backends/upower/xrandrxcbhelper.cpp
       daemon/backends/upower/xrandrxcbhelper.h
Copyright: 2012 by Alejandro Fiestas Olivares <afiestas@kde.org>
License: LGPL-2+
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2 or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Library General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

Files: daemon/backends/upower/udevqt.h
       daemon/backends/upower/udevqt_p.h
       daemon/backends/upower/udevqtclient.cpp
       daemon/backends/upower/udevqtclient.h
       daemon/backends/upower/udevqtdevice.cpp
       daemon/backends/upower/udevqtdevice.h
Copyright: 2009 Benjamin K. Stuhl <bks24@cornell.edu>
License: LGPL-2.1+ or (at your option) version 3, or any
    later version accepted by the membership of KDE e.V.
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) version 3, or any
 later version accepted by the membership of KDE e.V. (or its
 successor approved by the membership of KDE e.V.), which shall
 act as a proxy defined in Section 6 of version 3 of the license.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library. If not, see <http://www.gnu.org/licenses/>.
